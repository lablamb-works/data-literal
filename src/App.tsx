import React from 'react';
import ReactGA from 'react-ga';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { LandingPage } from './pages/LandingPage';
import { range } from 'lodash';
import './App.less';
import './styles/shared.less';

const trackingID = process.env.React_APP_GATrackingID ?? '';
ReactGA.initialize(trackingID, {
    debug: process.env.NODE_ENV === 'development' ? true : false,
});

const history = createBrowserHistory();
history.listen(location => {
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname);
});

const App: React.FC = () => {
    return (
        <main className="App">
            <Router history={history}>
                <Switch>
                    <Route path="/" children={<LandingPage />} />
                </Switch>
            </Router>
            <div id="container-ads">
                {range(1, 4).map(num => (
                    <div key={`ad-${num}`} className="ads">
                        320x250 Google AD
                    </div>
                ))}
            </div>
        </main>
    );
};

export default App;
