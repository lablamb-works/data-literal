import { FieldDataType } from '../Backend/Field';
export interface Field {
    fieldID: number;
    fieldKey: string;
}

export interface FieldProps extends Field {
    dataType?: FieldDataType;
    conditions?: string;
    option?: string;
}

export enum PipeSymbol {
    // Dollar = '$n',
    Percent = '%n',
}

export enum Format {
    Object = 'Object',
}

export enum Output {
    TXT = 'TXT',
    // JSON = 'JSON',
}
