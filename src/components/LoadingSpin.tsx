import React from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

export const LoadingSpin: React.FC = () => {
    const customIcon = <LoadingOutlined style={{ fontSize: '1rem' }} spin />;
    return <Spin style={{ color: '#FFFFFF' }} indicator={customIcon} />;
};
