import React from 'react';
import { Menu } from 'antd';
import { MenuProps } from 'antd/lib/menu';
import '../styles/DMenu.less';

interface DMenuProps extends MenuProps {
    prefixID: string;
    menuItems: Array<string | number>;
    value: string | number | undefined;
}

export const DMenu: React.FC<DMenuProps> = (props: DMenuProps) => {
    return (
        <Menu className="d-menu" theme="dark" onClick={props.onClick}>
            {props.menuItems.map((item: string | number) => (
                <Menu.Item
                    key={`${props.prefixID}-${item}`}
                    className={'d-menu-item ' + props.value && props.value === item ? 'selected' : ''}
                >
                    {item}
                </Menu.Item>
            ))}
        </Menu>
    );
};

export default DMenu;
