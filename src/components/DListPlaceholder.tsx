import React, { useMemo } from 'react';
import { Row, Col, Button } from 'antd';
import { convertStringToFields } from '../utils/template';

interface DListPlaceholderProps {
    placeholder: string;
}
convertStringToFields;
export const DListPlaceholder: React.FC<DListPlaceholderProps> = (props: DListPlaceholderProps) => {
    const fields = useMemo(() => {
        return convertStringToFields(props.placeholder);
    }, [props.placeholder]);

    return (
        <>
            {fields.map(field => (
                <div key={`placeholder-${field.fieldID}`} className="field-item">
                    <span className="anticon" />
                    <Row className="field-row" justify="space-between">
                        <Col className="field-type field-placeholder" span={12}>
                            <Button type="dashed">
                                <span className="field-id field-placeholder">{field.fieldID}</span>
                                <span style={{ width: '100%', textAlign: 'left' }} className="field-placeholder">
                                    Type
                                </span>
                            </Button>
                        </Col>
                        <Col className="field-example field-placeholder" span={12}>
                            <Button type="dashed">Example</Button>
                        </Col>
                    </Row>
                </div>
            ))}
        </>
    );
};
