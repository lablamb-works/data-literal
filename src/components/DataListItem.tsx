import React, { useContext, useState, useMemo } from 'react';
import { FieldPropsContext } from '../context/FieldPropsContext';
import { setDataType } from '../context/fieldProps/actions';
import { FieldProps } from '../model/model';
import { Row, Col, Input, AutoComplete } from 'antd';
import { DownOutlined, MinusCircleTwoTone } from '@ant-design/icons';
import { DDropdown } from './DDropdown';
import { enumToArray, enumToObjectArray } from '../utils/enum';
import { FieldDataType } from '../Backend/Field';
import '../styles/DataListItem.less';

interface DataListItemProps {
    fieldProps: FieldProps;
    onDelete: (event: React.MouseEvent<HTMLSpanElement>, fieldID: number, fieldKey: string) => void;
}
export const DataListItem: React.FC<DataListItemProps> = (props: DataListItemProps) => {
    const { dispatch } = useContext(FieldPropsContext);
    const [dataTypeOptions, setDataTypeOptions] = useState<{ value: string }[]>([]);
    const allDataTypeOptions = useMemo(() => {
        return enumToObjectArray<string>(FieldDataType);
    }, []);
    const [isLocked, setIsLocked] = useState(false);

    const handleSelect = (dataType: string) => {
        dispatch(setDataType(props.fieldProps.fieldID, dataType as FieldDataType));
    };

    const handleSearch = (value: string) => {
        const searchResult = allDataTypeOptions.filter(option => option.value.indexOf(value) > -1);
        setDataTypeOptions(!value ? [] : searchResult);
    };

    // to be implemented
    const lockAfterSelect = () => {
        console.log();
    };
    const unlock = () => {
        console.log();
    };

    return (
        <div className="field-item">
            <MinusCircleTwoTone
                twoToneColor={['#D89269', '#2C2C2C00']}
                onClick={event => props.onDelete(event, props.fieldProps.fieldID, props.fieldProps.fieldKey)}
            />
            <Row className="field-row" justify="space-between">
                <Col className="field-type" span={12}>
                    <span className="field-id">{props.fieldProps.fieldID}</span>
                    <AutoComplete
                        className="auto-complete"
                        options={dataTypeOptions}
                        onSelect={handleSelect}
                        onSearch={handleSearch}
                    >
                        <Input
                            placeholder="Type"
                            addonAfter={
                                <DDropdown
                                    prefixID={`${props.fieldProps.fieldID}-type`}
                                    icon={<DownOutlined />}
                                    menuItems={enumToArray(FieldDataType)}
                                    value={props.fieldProps.dataType}
                                    onSelect={handleSelect}
                                />
                            }
                        />
                    </AutoComplete>
                    {isLocked && <span className="field-lock">Lock</span>}
                </Col>
                <Col className="field-example" span={12}>
                    Example
                </Col>
            </Row>
        </div>
    );
};
