import React from 'react';
import { Dropdown } from 'antd';
import { ClickParam } from 'antd/lib/menu';
import { DMenu } from './DMenu';
import '../styles/DDropdown.less';

export interface DDropdownProps<T> {
    prefixID: string;
    icon?: React.ReactElement;
    label?: T;
    menuItems: Array<T>;
    isValueShown?: boolean;
    value?: T;
    onSelect: (selectedValue: string) => void;
}

export const DDropdown: React.FC<DDropdownProps<string | number>> = (props: DDropdownProps<string | number>) => {
    const handleItemClick = (param: ClickParam) => {
        const selectedValue = param.key.split('-').pop();
        if (selectedValue) {
            props.onSelect(selectedValue);
        }
    };

    const handleBtnClick = (event: React.MouseEvent<HTMLSpanElement>) => {
        event.stopPropagation();
    };

    return (
        <Dropdown
            overlay={
                <DMenu
                    prefixID={props.prefixID}
                    menuItems={props.menuItems}
                    value={props.value}
                    onClick={handleItemClick}
                />
            }
        >
            <span id={`${props.prefixID}-btn`} className="d-dropdown-btn" onClick={handleBtnClick}>
                {props.icon ? props.icon : props.isValueShown && props.value ? props.value : props.label}
            </span>
        </Dropdown>
    );
};
