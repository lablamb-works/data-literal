import React, { useContext, useRef, useState } from 'react';
import { TemplateContext } from '../context/TemplateContext';
import { FieldPropsContext } from '../context/FieldPropsContext';
import { onChange, setSymbol, setFormat, setQty, setFieldsFromTemplate, setOutput } from '../context/template/actions';
import { Input, Button } from 'antd';
import { DDropdown } from './DDropdown';
import { LoadingSpin } from './LoadingSpin';
import { PipeSymbol, Format, Output } from '../model/model';
import { lint, validate } from '../utils/template';
import { enumToArray } from '../utils/enum';
import { Generator } from '../Backend/Generator';
import { Scanner } from '../Backend/Scanner';
import IconSymbol from '../assets/icons/icon-symbol.svg';
import IconFormat from '../assets/icons/icon-format.svg';
import '../styles/TemplateInput.less';

interface TemplateInputProps {
    placeholder: string;
}

const { TextArea } = Input;
export const TemplateInput: React.FC<TemplateInputProps> = (props: TemplateInputProps) => {
    const { state, dispatch } = useContext(TemplateContext);
    const fieldPropsContext = useContext(FieldPropsContext);
    const GenLinkRef = useRef<HTMLAnchorElement>(null);
    const [loading, setLoading] = useState(false);

    const handleOnChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        dispatch(onChange(event.currentTarget.value));
        dispatch(setFieldsFromTemplate(event.currentTarget.value.replace(/\s/g, '')));
    };

    const generateData = () => {
        setLoading(true);
        const scanner = new Scanner();
        const randData = new Generator().generateRandomData(fieldPropsContext.state.fieldProps, state.qty);
        randData.then(res => {
            const result: Array<string> = [];
            res.forEach(data => {
                const row = scanner.scanAndReplace(state.string, state.symbol, data);
                result.push(row);
            });
            const file = new Blob([`[${result.join(',\n')}]`], { type: 'text/plain' });
            GenLinkRef.current?.setAttribute('href', URL.createObjectURL(file));
            GenLinkRef.current?.setAttribute(
                'download',
                `DataLiteral_${new Date()
                    .toISOString()
                    .slice(0, 19)
                    .replace('T', '_')}.txt`,
            );
            setLoading(false);
            GenLinkRef.current?.click();
        });
    };

    return (
        <div className="template">
            <div className="reserved-area" />
            <div className="template-row">
                <div className="template-column-options">
                    <DDropdown
                        prefixID="symbol"
                        icon={<img className="template-icon" src={IconSymbol} />}
                        menuItems={enumToArray(PipeSymbol)}
                        value={state.symbol}
                        onSelect={symbol => {
                            dispatch(setSymbol(symbol as PipeSymbol));
                        }}
                    />
                    <DDropdown
                        prefixID="format"
                        icon={<img className="template-icon" src={IconFormat} />}
                        menuItems={enumToArray(Format)}
                        value={state.format}
                        onSelect={format => {
                            dispatch(setFormat(format as Format));
                        }}
                    />
                    <DDropdown
                        prefixID="output"
                        icon={undefined}
                        menuItems={enumToArray(Output)}
                        value={state.output}
                        isValueShown={true}
                        onSelect={fileFormat => {
                            dispatch(setOutput(fileFormat as Output));
                        }}
                    />
                    <DDropdown
                        prefixID="qty"
                        menuItems={[10, 50, 100, 200, 500, 1000]}
                        value={state.qty}
                        isValueShown={true}
                        onSelect={qty => {
                            dispatch(setQty(parseInt(qty, 10)));
                        }}
                    />
                </div>
                <TextArea
                    id="template-field"
                    value={lint(state.string)}
                    placeholder={lint(props.placeholder)}
                    onChange={handleOnChange}
                />
            </div>
            <div className="template-row">
                <Button
                    type="primary"
                    id="gen-btn"
                    onClick={generateData}
                    disabled={!validate(state.format, state.symbol, state.fields)}
                >
                    {loading ? <LoadingSpin /> : 'Generate'}
                </Button>
                <a ref={GenLinkRef} />
            </div>
        </div>
    );
};
