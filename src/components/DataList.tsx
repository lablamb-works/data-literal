import React, { useContext, useEffect, useRef } from 'react';
import { FieldPropsContext } from '../context/FieldPropsContext';
import { clearAll } from '../context/fieldProps/actions';
import { TemplateContext } from '../context/TemplateContext';
import { onChange, reset, setFieldsFromTemplate } from '../context/template/actions';
import { DataListItem } from './DataListItem';
import { DListPlaceholder } from './DListPlaceholder';
import { Button } from 'antd';
import { PlusCircleTwoTone } from '@ant-design/icons';
import '../styles/DataList.less';

interface DataListProps {
    placeholder: string;
}

export const DataList: React.FC<DataListProps> = (props: DataListProps) => {
    const { state, dispatch } = useContext(FieldPropsContext);
    const templateContext = useContext(TemplateContext);
    const dataFieldRef = useRef<HTMLDivElement>(null);

    const addNewField = () => {
        const str = `field_${templateContext.state.fields.length + 1}:%${templateContext.state.fields.length + 1},}`;
        let newValue = '';
        if (!templateContext.state.string) {
            newValue = '{' + str;
        } else {
            newValue = templateContext.state.string.replace(/\s/g, '').replace(/(?<=[{,])\s*}$/g, str);
        }
        templateContext.dispatch(onChange(newValue));
        templateContext.dispatch(setFieldsFromTemplate(newValue));
    };

    const deleteField = (event: React.MouseEvent<HTMLSpanElement>, fieldID: number, fieldKey: string) => {
        if (templateContext.state.fields.length === 1) {
            dispatch(clearAll());
            templateContext.dispatch(reset());
        } else {
            const newString = templateContext.state.string
                .replace(/\s/g, '')
                .replace(`${fieldKey}:${templateContext.state.symbol[0]}${fieldID},`, '');
            templateContext.dispatch(onChange(newString));
            templateContext.dispatch(setFieldsFromTemplate(newString));
        }
    };

    useEffect(() => {
        const fieldMaxHeight = window.innerHeight - 40 - 70 - 30 - 2 * 32; // App margin: 40; Heading: 70; Footer: 30; Button row Height: 32
        if (dataFieldRef.current?.clientHeight && dataFieldRef.current?.clientHeight + 32 > fieldMaxHeight) {
            dataFieldRef.current.scrollTop = dataFieldRef.current?.scrollHeight;
        }
        const templateFieldNode = document.getElementById('template-field');
        if (templateFieldNode?.clientHeight && templateFieldNode?.clientHeight + 32 > fieldMaxHeight) {
            templateFieldNode.scrollTop = templateFieldNode?.scrollHeight;
        }
    }, [state.fieldProps.length]);

    return (
        <div className="field">
            <div className="reserved-area" />
            <div className="field-props" ref={dataFieldRef}>
                {state.fieldProps.length === 0 ? (
                    <DListPlaceholder placeholder={props.placeholder} />
                ) : (
                    state.fieldProps.map((field, index) => (
                        <DataListItem key={`data-${index}`} fieldProps={field} onDelete={deleteField} />
                    ))
                )}
            </div>
            <Button
                type="primary"
                className="field-add-btn"
                onClick={addNewField}
                icon={<PlusCircleTwoTone twoToneColor={['#FFFFFF', '#FFFFFF00']} />}
            />
        </div>
    );
};
