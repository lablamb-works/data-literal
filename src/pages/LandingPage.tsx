import React, { useEffect, useState } from 'react';
import { TemplateContextProvider } from '../context/TemplateContext';
import { FieldPropsContextProvider } from '../context/FieldPropsContext';
import { TemplateInput } from '../components/TemplateInput';
import { DataList } from '../components/DataList';
import { fetchPublicText } from '../utils/fetch';
import '../styles/LandingPage.less';

export const LandingPage: React.FC = () => {
    const [placeholder, setPlaceholder] = useState<string>('');
    useEffect(() => {
        (async () => {
            setPlaceholder(await fetchPublicText('./sample-template.txt'));
        })();
    }, []);
    return (
        <div id="container-main">
            <div className="heading">
                <img src="./DataLiteralLogo.png" alt="logo" width="350" height="46" />
                {/* <span className="primary">{ title } </span> */}
                {/* <img src={} alt="github" />
                        <img src={} alt="tutorial" /> */}
            </div>
            <div id="container-workspace">
                <TemplateContextProvider>
                    <FieldPropsContextProvider>
                        <TemplateInput placeholder={placeholder} />
                        <DataList placeholder={placeholder} />
                    </FieldPropsContextProvider>
                </TemplateContextProvider>
            </div>
            <div className="footer">
                @Copyright | <span>Terms & Condition</span> | <span>Support</span> | <span>Privacy Policy</span>
            </div>
        </div>
    );
};
