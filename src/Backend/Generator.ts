import { FieldDataType } from './Field';
import { FieldProps } from '../model/model';

export class Generator {
    async generateRandomData(fieldProps: FieldProps[], numOfResult: number): Promise<string[][]> {
        const result: string[][] = [];

        for (let i = 0; i < numOfResult; i++) {
            const tempArr: string[] = [];

            for (const field of fieldProps) {
                const filePath = (() => {
                    switch (field.dataType) {
                        case FieldDataType.Address:
                            return '../Data/Address_us.txt';
                        case FieldDataType.FirstNameEN:
                            return '../Data/FirstNames_en.txt';
                        case FieldDataType.LastNameEN:
                            return '../Data/LastNames_en.txt';
                        default:
                            return null;
                    }
                })();

                if (filePath === null) {
                    let randElement = '';
                    switch (field.dataType) {
                        case FieldDataType.Date:
                            const start = new Date(1970, 1, 1);
                            const end = new Date();
                            randElement = new Date(
                                start.getTime() + Math.random() * (end.getTime() - start.getTime()),
                            ).toString();
                            break;
                        case FieldDataType.Integer:
                            randElement = Math.round(Math.random() * 1000).toString();
                            break;
                        case FieldDataType.Double:
                            break;
                        case FieldDataType.Regex:
                            break;
                        default:
                            break;
                    }
                    tempArr.push(randElement);
                } else {
                    await fetch(filePath)
                        .then(res => {
                            return res.text();
                        })
                        .then(txt => {
                            const randData = txt.split('\n');
                            const randomElement = randData[Math.floor(Math.random() * randData.length)];
                            tempArr.push(randomElement);
                        });
                }
            }
            result[i] = tempArr;
        }
        const delay = (time: number) =>
            new Promise(res => {
                console.log('simulating delay');
                setTimeout(res, time);
            });
        await delay(1000);
        return result;
    }
}
