import { PipeSymbol } from '../model/model';

export class Scanner {
    scanForSymbols(template: string, symbol: PipeSymbol): number[] {
        const result: number[] = [];

        for (let i = 0; i < template.length; i++) {
            if (template[i] === symbol[0]) {
                let k = 1;
                let fieldNumStr = '';
                while (k < template.length) {
                    if (isNaN(Number(template[i + k]))) {
                        break;
                    }
                    fieldNumStr += template[i + k];
                    k += 1;
                }
                if (fieldNumStr !== '') {
                    result.push(Number(fieldNumStr));
                    i += fieldNumStr.length;
                }
            }
        }
        return [];
    }

    scanAndReplace(template: string, symbol: PipeSymbol, replaceValues: string[]): string {
        let result = '';

        for (let i = 0; i < template.length; i++) {
            if (template[i] === symbol[0]) {
                let k = 1;
                let fieldNumStr = '';
                while (k < template.length) {
                    if (isNaN(Number(template[i + k]))) {
                        break;
                    }
                    fieldNumStr += template[i + k];
                    k += 1;
                }
                if (fieldNumStr !== '') {
                    result += replaceValues[Number(fieldNumStr) - 1];
                    i += fieldNumStr.length;
                } else {
                    result += template[i];
                }
            } else {
                result += template[i];
            }
        }
        return result;
    }
}
