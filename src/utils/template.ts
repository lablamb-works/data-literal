import { Field, PipeSymbol } from '../model/model';
import { Scanner } from '../Backend/Scanner';

export const lint = (template: string) => {
    const result = template
        .replace(/{\s*/g, '{\n\t')
        .replace(/,\s*(?!})/g, ',\n\t')
        .replace(/:\s*(?!})/g, ': ')
        .replace(/(?<=,)\s*}.?$/g, '\n}');
    return result;
};

export const normalize = (template: string) => {
    return template.replace(/\s/g, '').replace(/,*}/g, ',}');
};

export const validate = (template: string, symbol: PipeSymbol, fields: Field[]) => {
    const symbols = new Scanner().scanForSymbols(template, symbol);

    if (symbols.length == 0 || fields.length == 0) {
        return false;
    }

    if (symbols.length !== fields.length) {
        return false;
    }

    const fieldIds = fields.map(field => {
        return field.fieldID;
    });

    const uniqueFields = new Set<number>(fieldIds);
    for (let i = 0; i < symbols.length; i++) {
        if (!uniqueFields.has(symbols[i])) {
            return false;
        }
    }

    return true;
};

export const convertStringToFields = (string: string) => {
    const str = normalize(string);
    const fieldKeys = str.match(/(?<=[{,])\w*\d*(?=:)/g) ?? [];
    const fieldIDs = str.match(/(?<=[%$])\w*\d*(?=[,}])/g) ?? [];
    const fields: Field[] = [];
    fieldIDs.forEach((fieldID: string, index: number) => {
        fields.push({ fieldID: index, fieldKey: fieldKeys[index] });
    });
    return fields;
};
