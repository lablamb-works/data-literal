// import { FieldDataType } from '../Backend/Field';

export const enumToArray = (enumerate: { [key: string]: string }): string[] => {
    return Object.values(enumerate);
};

export const enumToObjectArray = <T>(enumerate: { [key: string]: T }) => {
    const result: { value: T }[] = [];
    Object.values(enumerate).forEach((enumValue: T) => {
        result.push({ value: enumValue });
    });
    return result;
};
