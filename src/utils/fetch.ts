export const fetchPublicText = async (path: string) => {
    const res = await fetch(path, { method: 'GET' });
    return res.status === 200 ? await res.text() : 'Fail to fetch sample!';
};

export const delay = (time: number) =>
    new Promise(res => {
        console.log('simulating delay');
        setTimeout(res, time);
    });
