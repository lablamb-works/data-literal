import React, { Dispatch, useReducer, useContext, useEffect } from 'react';
import { FieldPropsState } from './fieldProps/state';
import { FieldPropsAction, setFieldsFromContext } from './fieldProps/actions';
import { FieldPropsReducer } from './fieldProps/reducer';
import { TemplateContext } from './TemplateContext';

interface FieldPropsContext {
    state: FieldPropsState;
    dispatch: Dispatch<FieldPropsAction>;
}

const initialFieldValue: FieldPropsState = {
    fieldProps: [],
};

export const FieldPropsContext = React.createContext<FieldPropsContext>({
    state: initialFieldValue,
    dispatch: () => ({}),
});
FieldPropsContext.displayName = 'FieldPropsContext';

interface FieldPropsContextProviderProps {
    children: React.ReactNode;
}

export const FieldPropsContextProvider = (props: FieldPropsContextProviderProps) => {
    const [state, dispatch] = useReducer(FieldPropsReducer, initialFieldValue);
    const templateContext = useContext(TemplateContext);
    useEffect(() => {
        dispatch(setFieldsFromContext(templateContext.state.fields));
    }, [templateContext.state.fields]);

    return <FieldPropsContext.Provider value={{ state, dispatch }}>{props.children}</FieldPropsContext.Provider>;
};
