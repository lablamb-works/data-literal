import { FieldPropsState } from './state';
import { FieldPropsAction } from './actions';

export const FieldPropsReducer = (state: FieldPropsState, action: FieldPropsAction) => {
    switch (action.type) {
        case '@@FIELD_PROPS/SET_FIELDS_FROM_CONTEXT':
            return {
                ...state,
                fieldProps: action.fields,
            };
        case '@@FIELD_PROPS/SET_DATA_TYPE':
            const newFields = [...state.fieldProps];
            const targetIndex = newFields.findIndex(datum => datum.fieldID === action.fieldID);
            newFields[targetIndex].dataType = action.dataType;
            return {
                ...state,
                fieldProps: newFields,
            };
        case '@@FIELD_PROPS/CLEAR_ALL':
            return {
                ...state,
                fieldProps: [],
            };
        default:
            return state;
    }
};
