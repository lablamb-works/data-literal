import { FieldDataType } from '../../Backend/Field';
import { Field } from '../../model/model';

export const setFieldsFromContext = (fields: Field[]) => {
    return {
        type: '@@FIELD_PROPS/SET_FIELDS_FROM_CONTEXT' as '@@FIELD_PROPS/SET_FIELDS_FROM_CONTEXT',
        fields,
    };
};

export const setDataType = (fieldID: number, dataType: FieldDataType) => {
    return {
        type: '@@FIELD_PROPS/SET_DATA_TYPE' as '@@FIELD_PROPS/SET_DATA_TYPE',
        fieldID,
        dataType,
    };
};

export const setConditions = (fieldID: string, conditions: string) => {
    return {
        type: '@@FIELD_PROPS/SET_CONDITIONS' as '@@FIELD_PROPS/SET_CONDITIONS',
        fieldID,
        conditions,
    };
};

export const setOptions = (fieldID: string, options: string) => {
    return {
        type: '@@FIELD_PROPS/SET_OPTION' as '@@FIELD_PROPS/SET_OPTION',
        fieldID,
        options,
    };
};

export const clearAll = () => {
    return {
        type: '@@FIELD_PROPS/CLEAR_ALL' as '@@FIELD_PROPS/CLEAR_ALL',
    };
};

type FieldPropsActionCreator =
    | typeof setFieldsFromContext
    | typeof setDataType
    | typeof setConditions
    | typeof setOptions
    | typeof clearAll;

export type FieldPropsAction = ReturnType<FieldPropsActionCreator>;
