import { FieldProps } from '../../model/model';

export interface FieldPropsState {
    fieldProps: FieldProps[];
}
