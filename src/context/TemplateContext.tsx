import React, { Dispatch, useReducer } from 'react';
import { TemplateState } from './template/state';
import { TemplateAction } from './template/actions';
import { TemplateReducer } from './template/reducer';
import { PipeSymbol, Format, Output } from '../model/model';

interface TemplateContext {
    state: TemplateState;
    dispatch: Dispatch<TemplateAction>;
}

const initialTemplateValue: TemplateState = {
    symbol: PipeSymbol.Percent,
    format: Format.Object,
    string: '',
    fields: [],
    qty: 100,
    output: Output.TXT,
};

export const TemplateContext = React.createContext<TemplateContext>({
    state: initialTemplateValue,
    dispatch: () => ({}),
});
TemplateContext.displayName = 'TemplateContext';

interface TemplateContextProviderProps {
    children: React.ReactNode;
}

export const TemplateContextProvider = (props: TemplateContextProviderProps) => {
    const [state, dispatch] = useReducer(TemplateReducer, initialTemplateValue);
    return <TemplateContext.Provider value={{ state, dispatch }}>{props.children}</TemplateContext.Provider>;
};
