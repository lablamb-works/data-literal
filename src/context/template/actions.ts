import { PipeSymbol, Format, Output } from '../../model/model';

export const setSymbol = (symbol: PipeSymbol) => {
    return {
        type: '@@TEMPLATE/SET_SYMBOL' as '@@TEMPLATE/SET_SYMBOL',
        symbol,
    };
};

export const setFormat = (format: Format) => {
    return {
        type: '@@TEMPLATE/SET_FORMAT' as '@@TEMPLATE/SET_FORMAT',
        format,
    };
};

export const setQty = (qty: number) => {
    return {
        type: '@@TEMPLATE/SET_QUANTITY' as '@@TEMPLATE/SET_QUANTITY',
        qty,
    };
};

export const setOutput = (output: Output) => {
    return {
        type: '@@TEMPLATE/SET_OUTPUT' as '@@TEMPLATE/SET_OUTPUT',
        output,
    };
};

export const onChange = (string: string) => {
    return {
        type: '@@TEMPLATE/ON_CHANGE' as '@@TEMPLATE/ON_CHANGE',
        string,
    };
};

export const reset = () => {
    return {
        type: '@@TEMPLATE/RESET' as '@@TEMPLATE/RESET',
    };
};

export const setFieldsFromTemplate = (string: string) => {
    return {
        type: '@@TEMPLATE/SET_FIELDS_FROM_TEMPLATE' as '@@TEMPLATE/SET_FIELDS_FROM_TEMPLATE',
        string,
    };
};

type TemplateActionCreator =
    | typeof setSymbol
    | typeof setFormat
    | typeof setQty
    | typeof setOutput
    | typeof onChange
    | typeof reset
    | typeof setFieldsFromTemplate;

export type TemplateAction = ReturnType<TemplateActionCreator>;
