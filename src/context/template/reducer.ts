import { TemplateState } from './state';
import { TemplateAction } from './actions';
import { convertStringToFields } from '../../utils/template';

export const TemplateReducer = (state: TemplateState, action: TemplateAction) => {
    switch (action.type) {
        case '@@TEMPLATE/SET_SYMBOL':
            return {
                ...state,
                symbol: action.symbol,
            };
        case '@@TEMPLATE/SET_FORMAT':
            return {
                ...state,
                format: action.format,
            };
        case '@@TEMPLATE/SET_QUANTITY':
            return {
                ...state,
                qty: action.qty,
            };
        case '@@TEMPLATE/SET_OUTPUT':
            return {
                ...state,
                output: action.output,
            };
        case '@@TEMPLATE/ON_CHANGE':
            return {
                ...state,
                string: action.string,
            };
        case '@@TEMPLATE/RESET':
            return {
                ...state,
                string: '',
            };
        case '@@TEMPLATE/SET_FIELDS_FROM_TEMPLATE':
            return {
                ...state,
                fields: convertStringToFields(action.string),
            };

        default:
            return state;
    }
};
