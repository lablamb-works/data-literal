import { PipeSymbol, Field, Output } from '../../model/model';
import { Format } from '../../model/model';

export interface TemplateState {
    symbol: PipeSymbol;
    format: Format;
    string: string;
    fields: Field[];
    qty: number;
    output: Output;
}
